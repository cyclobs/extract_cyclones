from setuptools import setup, find_packages
import os
import sys
from pathlib import Path
import glob

install_requires = [
      'shapely',
      'pandas',
      'geopandas',
      'numpy',
      'xarray',
      'geoalchemy2',
      'geo_shapely',
      'netcdf4',
      'rioxarray'

]

setup(name='extract_cyclones',
      description='Package used to adapt SMAP data for a cyclone oriented usage.',
      url='https://gitlab.ifremer.fr/cyclobs/extract_cyclones',
      author = "Théo CEVAER",
      author_email = "theo.cevaer@ifremer.fr",
      license='GPL',
      packages=find_packages(),
      include_package_data=True,
      scripts=glob.glob('bin/**'),
      use_scm_version=True,
      setup_requires=['setuptools_scm'],
      install_requires=install_requires,
      zip_safe = False
)