#!/usr/bin/env python

import argparse
import logging
import datetime
import os
import sys

#logging.basicConfig()
logger = logging.getLogger(os.path.basename(__file__))
#logger.setLevel(logging.INFO)


def extract_date_from_filename(filename):
    logger.debug(f"Filename: {filename}")
    date_part = "-".join(filename.split("_")[4:7])
    return datetime.datetime.strptime(date_part, "%Y-%m-%d")


if __name__ == "__main__":
    description = """
        Read SMAP netCDF files from a directory, extract the wind data of cyclones and save it into a new netCDF file.
        """
    parser = argparse.ArgumentParser(description=description)

    parser.add_argument("--debug", action="store_true", default=False, help="Run in debug mode (verbose output)")
    parser.add_argument("-i", "--input", action="store", type=str, required=True, nargs="+",
                        help="Input files from which extract cyclone data")
    parser.add_argument("-o", "--output", action="store", type=str, default="./output_test",
                        help="Output path where files will be written")
    args = parser.parse_args()

    if sys.gettrace():
        logger.setLevel(logging.DEBUG)
        logging.basicConfig(level=logging.DEBUG)

    if args.debug:
        logger.setLevel(logging.DEBUG)
        logging.basicConfig(level=logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)
        logging.basicConfig(level=logging.INFO)

    from extract_cyclones import process_file

    #attrs_to_del = ["aprrox_local_equatorial_crossing_time", "swath_sector", "first_orbit", "last_orbit"]

    #var_attr_edit = {"lon": {"valid_min": -180, "valid_max": 180}}

    # Attributes to add to the smap netcdf
    attrs = {"Conventions": "CF-1.6",
             "title": "SMAP ocean surface wind speed (10m above surface) cropped around Tropical Cyclone track",
             "institution": "IFREMER/LOPS",
             "reference": "https://cyclobs.ifremer.fr/app/about#smap_ref",
             "sourceReference": ("Meissner, T., L. Ricciardulli, and F. Wentz, 2018: Remote Sensing Systems SMAP daily"
                                 "Sea Surface Winds Speeds on 0.25 deg grid, Version 01.0. [NRT or FINAL]. Remote "
                                 "Sensing Systems, Santa Rosa, CA. Available online at www.remss.com/missions/smap/"),
             "missionName": "Soil Moisture Active Passive",
             "wind": {"_FillValue": -9999}
             }

    attrs_to_del = {"wind": ["missing_value"]}
    attrs_rename = {"version": "sourceProductVersion"}

    for f in args.input:
        # TODO generate filename_format depending on input filename
        process_file(file=f, output_path=args.output, extract_date_func=extract_date_from_filename,
                     var_to_del=["minute"],
                     attrs=attrs,
                     attrs_to_del=attrs_to_del,
                     attrs_rename=attrs_rename,
                     wind_col="wind", time_col="minute",
                     lat_col="lat",
                     lon_col="lon", pass_col="node", pass_width=1000,
                     filename_format="RSS_smap_wind_<start_date>_<stop_date>_<sid>.nc")
