#!/bin/bash

output_path=$3
file_path=$5
filename="${file_path##*/}"
filename_no_ext="${filename%.*}"

# TODO put report file in same dir as generated files

file_report_dir="$output_path/report/$filename_no_ext"
mkdir -p "$file_report_dir"

$1 "${@:2}" > "$file_report_dir/Extract.log" 2>&1
status=$?
echo $status > "$file_report_dir/Extract.status"

cat "$file_report_dir/Extract.log"

exit $status
