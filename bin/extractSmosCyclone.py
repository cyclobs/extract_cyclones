#!/usr/bin/env python

import argparse
import logging
import datetime
import sys
import numpy as np

# logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


# logger.setLevel(logging.INFO)


def extract_date_from_filename(filename):
    logger.debug(f"Filename: {filename}")
    date_part = filename.split("_")[4]
    return datetime.datetime.strptime(date_part, "%Y%m%d")


def set_nan_low_quality(dataset):
    """
    Sets to NaN the dataset values corresponding to a SMOS pixel quality level > 1
    The data with quality_level > 1 is considered bad quality thus is removed.
    This is specified in SMOS daily product specification : https://www.smosstorm.org/content/download/139283/file/SMOS_WIND_RM_TN_v1.1_20200304_signed.pdf

    Parameters
    ----------
    dataset : xarray.Dataset already containing data cropped around cyclone.

    Returns
    -------
    xarray.Dataset
        Same dataset but with NaN sets to all variables where variable quality_level is > 1
    """

    quality_col = "quality_level"
    # set nan to quality levels > 1 because
    with np.errstate(invalid='ignore'):  # ignoring warnings caused by Nans in array
        dataset[quality_col] = dataset[quality_col].where(dataset[quality_col].data <= 1)

    # Sets equivalents NaNs to other variables
    for var in dataset.data_vars:
        if var != "measurement_time":  # Not updating measurement_time (variable doesn't accept np.nan and will be removed afterwards anyway)
            dataset[var].data[np.isnan(dataset[quality_col].data)] = np.nan

    return dataset


if __name__ == "__main__":
    description = """
        Read SMOS netCDF files from a directory, extract the wind data of cyclones and save it into a new netCDF file.
        """
    parser = argparse.ArgumentParser(description=description)

    parser.add_argument("--debug", action="store_true", default=False, help="Run in debug mode (verbose output)")
    parser.add_argument("-i", "--input", action="store", type=str, required=True, nargs="+",
                        help="Input files from which extract cyclone data")
    parser.add_argument("-o", "--output", action="store", type=str, default="./output_test",
                        help="Output path where files will be written")
    args = parser.parse_args()

    if sys.gettrace():
        logger.setLevel(logging.DEBUG)
        logging.basicConfig(level=logging.DEBUG)

    if args.debug:
        logger.setLevel(logging.DEBUG)
        logging.basicConfig(level=logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)
        logging.basicConfig(level=logging.INFO)

    # Attributes that'll be deleted
    # attrs_to_del = ["aprrox_local_equatorial_crossing_time", "swath_sector", "geospatial_bounds",
    #                "geospatial_lat_min", "geospatial_lat_max", "geospatial_lon_min", "geospatial_lon_max"]

    attrs = {"Conventions": "CF-1.6",
             "title": "SMOS ocean surface wind speed cropped around Tropical Cyclone track",
             "institution": "IFREMER/LOPS",
             "reference": "https://cyclobs.ifremer.fr/app/about#smos_ref",
             "sourceReference": ("Reul Nicolas, Tenerelli Joseph, Chapron Bertrand, Vandemark Doug, Quilfen "
                                 "Yves, Kerr Yann (2012). SMOS satellite L-band radiometer: A new capability for "
                                 "ocean surface remote sensing in hurricanes. Journal Of Geophysical Research-oceans, "
                                 "117. Publisher's official version :  http://dx.doi.org/10.1029/2011JC007474"),
             "missionName": "Soil Moisture and Ocean Salinity"
             }
    attrs_to_del = {"time": ["authority"], "lat": ["authority"], "lon": ["authority"], "wind_speed": ["authority"]}
    attrs_rename = {"product_version": "sourceProductVersion"}

    from extract_cyclones import process_file

    for f in args.input:
        # TODO generate filename_format depending on input filename
        process_file(file=f, output_path=args.output, extract_date_func=extract_date_from_filename,
                     var_to_del=["measurement_time", "time"], wind_col="wind_speed", attrs_to_del=attrs_to_del,
                     attrs=attrs,
                     attrs_rename=attrs_rename,
                     time_col="measurement_time",
                     lat_col="lat", lon_col="lon", pass_col=None, pass_width=1200,
                     filename_format="SM_OPER_MIR_<start_date>_<stop_date>_<sid>.nc", specfic_func=set_nan_low_quality)
