# extract_cyclones

Package used to adapt daily swath data for a cyclone oriented usage.

Provide scripts to extract cyclone data from daily swath NetCDF files.

It currently works with SMAP and SMOS daily data but is made to easily add a new mission.


## Installation

```
# Add cyclobs pypi server
python -m pip config --site set global.trusted-host br156-246
python -m pip config --site set global.extra-index-url http://br156-246:8080
python -m pip config --site set global.index http://br156-246:8080

# Install
pip install --pre extract_cyclones
```


## How it works

Each daily file is opened, its date is extracted.

Using the extracted date, a request is made to CyclObs database to retrieve all the cyclone track points for that day.

For each track point, we check if there is available matching data in the daily file (matching spatio-temporally). If there is, custom portion of the data is selected and written to a new NetCDF file. Before writing attributes are also deleted or created.

Note that to work, the script must have access to a working CyclObs database.
