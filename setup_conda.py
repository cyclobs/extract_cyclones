from setuptools import setup, find_packages
import glob

setup(name='extract_cyclones',
      description='Extract portion related to tropical cyclones from SMOS and SMAP passes (wind speed products)',
      url='https://gitlab.ifremer.fr/cyclobs/extract_cyclones',
      author="Théo CEVAER",
      author_email="theo.cevaer@ifremer.fr",
      license='GPL',
      packages=find_packages(),
      include_package_data=True,
      scripts=glob.glob('bin/**'),
      use_scm_version=True,
      setup_requires=['setuptools_scm'],
      zip_safe=False
      )
